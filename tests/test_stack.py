import pytest

from dsa.data_structures.stack import Stack, reverse_a_word


def test_stack_data_structure():
    """Test the stack data structure."""
    stack = Stack(stack_size=2)
    assert stack.isEmpty() is True
    assert stack.isFull() is False

    with pytest.raises(Exception) as exc:
        stack.pop()
    assert 'The stack is empty.' in exc.value.args[0]

    with pytest.raises(Exception) as exc:
        stack.peek()
    assert 'The stack is empty.' in exc.value.args[0]

    stack.push(1)
    assert stack.isEmpty() is False
    assert stack.isFull() is False
    assert stack.peek() == 1

    stack.push(5)
    assert stack.isEmpty() is False
    assert stack.isFull() is True
    assert stack.peek() == 5

    with pytest.raises(Exception) as exc:
        stack.push(10)
    assert 'The stack is full.' in exc.value.args[0]


def test_reverse_a_word():
    """Test reversing a word using a stack."""
    assert reverse_a_word('fury  road') == 'daor  yruf'
    assert reverse_a_word('') == ''
