from dsa.greedy_algorithms.activity_selection_problem import (
    iterative_activity_selection, recursive_activity_selection)


def test_activities_problem():
    si = [1,3,0,5,3,5,6,8,8,2,12]
    fi = [4,5,6,7,9,9,10,11,12,14,16]

    assert [1, 4, 8, 11] == iterative_activity_selection(
        si, fi) == recursive_activity_selection(si, fi)
