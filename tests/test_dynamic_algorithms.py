from dsa.dynamic_algorithms.fibonacci import fibonacci
from dsa.dynamic_algorithms.rod_cutting_problem import (
    dynamic_top_down_cut_rod_solution)


def test_dynamic_top_down_cut_rod_solution():
    # Rod on length one can only be sold wholesome
    assert (10, [1]) == dynamic_top_down_cut_rod_solution(1, [10])

    assert (10, [2, 2]) == dynamic_top_down_cut_rod_solution(4, [1, 5, 8, 9])


def test_fibonacci():
    # 1,1,2,3,5,8,..
    assert fibonacci(1) == 1
    assert fibonacci(3) == 2
    assert fibonacci(20) == 6765
