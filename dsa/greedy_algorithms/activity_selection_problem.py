"""Activity selection problem definition.

You are to create a program that maximizes the use of a conference hall.
Give two non empty arrays **si** and **fi** such that;
1. len(**si**) == len(**s2**)
2. **si** is a list of activity start times and **fi** a list of finish times such that
activity **i** runs from **si[i]** to **fi[i]**

Create a program that computes a maximum set of Non-overlapping activies that can be held
in the conference room within a day.

Assume that array **fi** is sorted in a monotonically increasing order
"""


def recursive_activity_selection(si, fi):
    """Recursively solve the activity selection problem."""
    # Ensure that the initial call sets the previos_activity index to -1.
    return _recursive_activity_selection_helper(si, fi, -1)


def _recursive_activity_selection_helper(si, fi, previous_activity):
    """Recursively defined helper for the activity selection problem."""
    for i in range(previous_activity + 1, len(si)):
        if previous_activity == -1 or si[i] >= fi[previous_activity]:
            return [i + 1] + _recursive_activity_selection_helper(si, fi, i)

    return []


def iterative_activity_selection(si, fi):
    """Iteratively solve the activity selection problem."""  # noqa: D401
    previous_activity = 0
    compatible_activities = [1]

    for i in range(1, len(si)):
        if si[i] >= fi[previous_activity]:
            compatible_activities.append(i + 1)
            previous_activity = i

    return compatible_activities
