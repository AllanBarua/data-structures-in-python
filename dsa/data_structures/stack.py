"""Implementation of the stack data structure."""
import math


class Stack(object):
    """Stack class."""

    def __init__(self, stack_size=None):
        """Initialize stack variables."""
        self.__elements = []
        self.__top = -1
        self.__stack_size = stack_size or math.inf
        super().__init__()

    def isEmpty(self):
        """Check whether the stack is empty."""
        return self.__top == -1

    def isFull(self):
        """Check whether the stack is full."""
        return self.__stack_size - self.__top == 1

    def push(self, item):
        """Add an item to the stack."""
        if self.isFull():
            raise Exception('The stack is full.')
        else:
            self.__elements.append(item)
            self.__top += 1

    def pop(self):
        """Remove an item from the stack."""
        if self.isEmpty():
            raise Exception('The stack is empty.')
        else:
            el = self.__elements.pop()
            self.__top -= 1
            return el

    def peek(self):
        """Return the item at the top of the stack."""
        if self.isEmpty():
            raise Exception('The stack is empty.')
        else:
            return self.__elements[self.__top]


def reverse_a_word(word):
    """Reverse a word."""
    stack = Stack(stack_size=len(word))
    for letter in word:
        stack.push(letter)

    # While creating a string from constituent characters,
    # avoid appending characters to an already constructed string.
    # This causes the creation of a new string object everytime reducing efficicency.
    # https://docs.python-guide.org/writing/structure/#mutable-and-immutable-types
    reversed_list = []
    while not stack.isEmpty():
        reversed_list.append(stack.pop())

    reversed_word = ''.join(reversed_list)
    return reversed_word
