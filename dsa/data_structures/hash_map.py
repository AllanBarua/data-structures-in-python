import abc
import logging
from unittest.mock import patch

LOGGER = logging.getLogger(__name__)


class HashTableInterface(metaclass=abc.ABCMeta):
    """
    An interface defining methods to be implemented by
    all hashtable classes.
    """

    @abc.abstractstaticmethod
    def _hash(self, key: str) -> str:
        """Retrive a hash code for the given key."""
        pass

    @abc.abstractstaticmethod
    def insert(self, value) -> None:
        """Insert an element to the hash table."""
        pass

    @abc.abstractstaticmethod
    def delete(self, value) -> None:
        """Remove an element from the hash table."""
        pass

    @abc.abstractstaticmethod
    def exists(self, value) -> bool:
        """Check whether an element exists in the hash table."""
        pass


class Node:

    def __init__(self, value):
        self.left = None
        self.right = None
        self.value = value

    def __str__(self):
        return str(self.value)


class OpenHashedTable(HashTableInterface):
    """Collision is resolved by open hashing (Chaining)."""

    def __init__(self, size):
        msg = 'The size of the hash table should be a positive integer.'
        assert size > 0, msg

        self._hash_table = [None]*size
        self.size = size

    def _hash(self, key):
        return key % self.size

    def insert(self, value):
        hash_code = self._hash(value)
        head = self._hash_table[hash_code]

        if self.exists(value):
            LOGGER.info('Element %s already exists.', value)

        elif head:
            new_node = Node(value)
            new_node.right = head
            new_node.right.left = new_node
            self._hash_table[hash_code] = new_node

        else:
            self._hash_table[hash_code] = Node(value)

    def delete(self, value):
        hash_code = self._hash(value)
        head = self._hash_table[hash_code]

        while head:
            if head.value != value:
                head = head.right
                continue

            # If there is no node trailing the current node,
            # then the current node is the head.
            if not head.left:
                if head.right and head.right.left:
                    head.right.left = None

                self._hash_table[hash_code] = head.right
                del head
                return

            head.left.right = head.right
            if head.right:
                head.left.right.left = head.left.right

            del head
            return

        LOGGER.info('Element %s missing in the hash table.', value)

    def exists(self, value):
        hash_code = self._hash(value)
        head = self._hash_table[hash_code]

        while head:
            if head.value == value:
                return True

            head = head.right

        return False

    def display_table(self, hash_code):
        values = []
        head = self._hash_table[hash_code]

        while head:
            values.append(head.value)
            head = head.right

        return values


@patch('dsa.data_structures.hash_map.LOGGER.info')
def test_insert(logger_info):
    table = OpenHashedTable(size=5)
    assert table.exists(1) is False

    # Inserts 1 at slot 1.
    table.insert(1)
    assert table.exists(1) is True

    # Inserts 3 at slot 3.
    assert table.exists(3) is False
    table.insert(3)
    assert table.exists(3) is True

    # Collision on insert
    # 1 and 6 all map to the same hash code.
    assert table.exists(6) is False
    table.insert(6)
    assert table.exists(6) is True

    # Insert lready existing value.
    assert table.exists(6) is True
    table.insert(6)
    logger_info.assert_called_once_with(
        'Element %s already exists.', 6
    )
    assert table.display_table(1) == [6, 1]


@patch('dsa.data_structures.hash_map.LOGGER.info')
def test_delete(logger_info):
    table = OpenHashedTable(size=5)

    table.insert(1)
    assert table.exists(1) is True
    table.delete(1)
    assert table.exists(1) is False

    # Build a linked list on slot 0
    table.insert(15)
    table.insert(10)
    table.insert(5)
    table.insert(0)
    assert table.display_table(0) == [0, 5, 10, 15]

    # Delete middle element.
    table.delete(5)
    assert table.exists(5) is False
    assert table.display_table(0) == [0, 10, 15]

    # Delete last element.
    table.delete(15)
    assert table.exists(15) is False
    assert table.display_table(0) == [0, 10]

    # Delete first element.
    table.delete(0)
    assert table.exists(0) is False
    assert table.display_table(0) == [10]

    # Delete only element.
    table.delete(10)
    assert table.exists(10) is False
    assert table.display_table(0) == []

    # Delete None existant value.
    assert table.exists(3) is False
    table.delete(3)
    assert table.exists(3) is False
    assert table.display_table(3) == []
    logger_info.assert_called_once_with(
        'Element %s missing in the hash table.', 3
    )
