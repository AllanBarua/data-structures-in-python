from dsa.data_structures.trees.binary_tree import Node


class AVLTree(Node):
    """
    A height balanced binary search tree i.e;
    - It is a binary search tree
    - The difference in heights between the two subtrees
    is always maintained at 1 or 0. (Balance factor)
    """

    def insert(node, value_to_insert):
        """Insert an element into the AVL tree."""
        if node.value == value_to_insert:
            return node

        # Dynamically determine what side of the tree to traverse and the
        # node height to modify after insertion.
        side, side_height = (
            ('left', 'left_height') if value_to_insert < node.value else
            ('right', 'right_height')
        )

        child_node = getattr(node, side)
        if not child_node:
            setattr(node, side, AVLTree(value_to_insert))

        else:
            setattr(node, side, child_node.insert(value_to_insert))

        subtree = getattr(node, side)
        setattr(node, side_height, subtree.height + 1)

        return node.balance()

    def delete(node, value_to_delete):
        """Delete an element from the AVL tree."""
        if node.value == value_to_delete:
            if node.left:
                node.value = node.left.get_max_value()
                node.left = node.left.delete(node.value)
            elif node.right:
                node = node.right
            else:
                return None

            # Calculate the height of the left and right subtrees
            # for the replacement node.
            node.calculate_height()

            # Balance the node if imbalanced.
            return node.balance()

        side, side_height = (
            ('left', 'left_height') if value_to_delete < node.value
            else ('right', 'right_height')
        )

        child_node = getattr(node, side)
        if child_node:
            setattr(node, side, child_node.delete(value_to_delete))
        else:
            return node

        # Only determine the height of the subtree that might
        # have changed due to the deletion.
        subtree = getattr(node, side)
        subtree_height = subtree.height + 1 if subtree else 0
        setattr(node, side_height, subtree_height)

        # Balance the node if imbalanced.
        return node.balance()

    def balance(node):
        """Balance an imbalanced node."""
        balance_factor = (
            node.left_height - node.right_height)

        # An already balanced node.
        if balance_factor in {-1, 0, 1}:
            return node

        # Left-heavy binary search tree.
        # Perform either a Right rotation or a
        # Left-Right rotation to balance the tree.
        if balance_factor == 2:
            if node.left.left is not None:
                return node.right_rotate()
            else:
                node.left = node.left.left_rotate()
                return node.right_rotate()

        # Right-heavy binary search tree.
        # Perform either a Left rotation or a
        # Right-Left rotation to balance the tree.
        elif balance_factor == -2:
            if node.right.right is not None:
                return node.left_rotate()
            else:
                node.right = node.right.right_rotate()
                return node.left_rotate()

        else:
            raise Exception(
                f'Unknown balance factor for node {node.value}')

    def left_rotate(current_node):
        """Perform left rotation on a right-heavy BST."""
        right_subtree = current_node.right
        current_node.right = right_subtree.left
        right_subtree.left = current_node

        # Modify node heights
        current_node.right_height = right_subtree.left_height
        right_subtree.left_height = 1 + max(
            current_node.right_height, current_node.left_height)

        return right_subtree

    def right_rotate(current_node):
        """Perform right rotation on a left-heavy BST."""
        left_subtree = current_node.left
        current_node.left = left_subtree.right
        left_subtree.right = current_node

        # Modify node heights
        current_node.left_height = left_subtree.right_height
        left_subtree.right_height = 1 + max(
            current_node.right_height, current_node.left_height)

        return left_subtree

    def calculate_height(node):
        """Calculate a node's height."""
        node.left_height = (
            node.left.calculate_height() + 1
            if node.left else 0)

        node.right_height = (
            node.right.calculate_height() + 1
            if node.right else 0
        )

        return node.height

    def get_max_value(node):
        """Get the largest element in the tree."""
        if not node.right:
            return node.value

        node.right.get_max_value()
