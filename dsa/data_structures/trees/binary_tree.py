class UnequalSubTrees(Exception):
    pass


class UnbalancedTree(Exception):
    pass


class Node:
    """Basic building block for a binary tree."""

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

        # Meta data
        self.left_height = 0
        self.right_height = 0

    @property
    def height(self):
        """Calculate the height of a binary tree."""
        return max(self.left_height, self.right_height)

    @classmethod
    def count_nodes(cls, root):
        """Count the number of nodes in a binary tree."""
        if not root:
            return 0

        left_subtree_count = cls.count_nodes(root.left)
        right_subtree_count = cls.count_nodes(root.right)

        return 1 + left_subtree_count + right_subtree_count

    def __str__(self):
        msg = (
            f'Node Value: {self.value}\n'
            f'Left Height: {self.left_height}\n'
            f'Right Height: {self.right_height}\n'
            f'Left Child: {self.left is not None}\n'
            f'Right Child: {self.right is not None}'
        )
        return msg


class BinaryTree(Node):
    """A tree structure where each node has a maximum of 2 children."""

    @classmethod
    def build_a_binary_tree(cls):
        """Manually create a binary tree.
                    1
                  .  .
                 .    .
                12     9
               .  .
              .    .
             5      6
        """
        root = cls(1)
        node_2 = cls(12)
        node_3 = cls(9)
        node_4 = cls(5)
        node_5 = cls(6)

        root.left = node_2
        root.right = node_3

        node_2.left = node_4
        node_2.right = node_5

        return root

    @classmethod
    def build_a_complete_binary_tree(cls, elements, index=0):
        """Create a complete binary tree given a list of elements."""
        if index >= len(elements):
            return None

        node = cls(elements[index])
        l_index = 2 * index + 1
        r_index = 2 * index + 2

        node.left = cls.build_a_complete_binary_tree(elements, l_index)
        node.right = cls.build_a_complete_binary_tree(elements, r_index)

        return node

    def traverse_inorder(self, elements=None):
        """Left -> Root -> Right."""
        elements = elements or []

        if self.left:
            elements = self.left.traverse_inorder(elements)

        elements.append(self.value)

        if self.right:
            elements = self.right.traverse_inorder(elements)

        return elements

    def traverse_preorder(self, elements=None):
        """Root -> Left -> Right."""
        elements = elements or []
        elements.append(self.value)

        if self.left:
            elements = self.left.traverse_inorder(elements)

        if self.right:
            elements = self.right.traverse_inorder(elements)

        return elements

    def traverse_postorder(self, elements=None):
        """Left -> Right -> Root."""
        elements = elements or []

        if self.left:
            elements = self.left.traverse_inorder(elements)

        if self.right:
            elements = self.right.traverse_inorder(elements)

        elements.append(self.value)

        return elements

    def is_a_binary_tree(self):
        """
        A binary tree is a tree structure whose internal nodes
        have a maximum of two child nodes.
        """
        return isinstance(self, Node)

    def is_a_full_binary_tree(self):
        """
        A full binary tree is a tree structure where each internal
        node has either 0 or 2 child nodes. It is also called a
        proper binary tree.
        """
        if self.left and self.right:
            return (
                self.left.is_a_full_binary_tree()
                and self.right.is_a_full_binary_tree()
            )

        if self.left or self.right:
            return False

        return True

    def is_a_perfect_binary_tree(self):
        """
        A perfect binary tree is a tree structure where every
        internal node has exactly 2 child nodes and all the leaf
        nodes have the same depth (are in the same level).
        """

        def compare_subtree_heights(self):
            if self.left and self.right:
                l_height = compare_subtree_heights(self.left)
                r_height = compare_subtree_heights(self.right)

                if l_height == r_height:
                    return l_height + 1
                else:
                    raise UnequalSubTrees

            if self.left or self.right:
                raise UnequalSubTrees

            return 0

        try:
            compare_subtree_heights(self)
        except UnequalSubTrees:
            return False
        else:
            return True

    def is_a_complete_binary_tree(root):
        """
        A Complete binary tree is a binary tree where all internal
        levels are complete except for the deepest level that can
        have nodes filled from left to right.
        """
        number_of_nodes = Node.count_nodes(root)

        def compare_subtrees(root, index):
            if not root:
                return True

            if index >= number_of_nodes:
                return False

            return (
                compare_subtrees(root.left, 2 * index + 1)
                and compare_subtrees(root.right, 2 * index + 2)
            )

        return compare_subtrees(root, 0)

    def is_a_balanced_binary_tree(root):
        """
        A balanced binary tree is a binary tree where the diffence
        in height of the left and the right subtree is 1 or 0 for
        all the nodes.
        It is also called the height balanced binary tree.
        """

        def compare_subtrees(root):
            if not root:
                return 0

            left_height = compare_subtrees(root.left)
            right_height = compare_subtrees(root.right)

            if abs(left_height - right_height) > 1:
                raise UnbalancedTree

            return max(left_height, right_height) + 1

        try:
            compare_subtrees(root)
        except UnbalancedTree:
            return False
        else:
            return True

    def is_a_degenerate_tree(self):
        """
        A binary tree where each internal node has only one child node.
        - Also known as a pathological tree.
        """
        if self.right and self.left:
            return False

        if not self.right and not self.left:
            return True

        side = 'left' if self.left else 'right'
        return True and getattr(self, side).is_a_degenerate_tree()

    def is_a_left_skewed_tree(self):
        """A pathological tree dominated by left facing nodes only."""
        if self.right:
            return False

        if self.left:
            return True and self.left.is_a_left_skewed_tree()

        return True

    def is_a_right_skewed_tree(self):
        """A pathological tree dominated by right facing nodes only."""
        if self.left:
            return False

        if self.right:
            return True and self.right.is_a_right_skewed_tree()

        return True
