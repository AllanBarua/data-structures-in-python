from dsa.data_structures.trees.binary_tree import Node


class BinarySearchTree(Node):
    """
    A binary search tree is characterized by the fact that;
    - All nodes in the left subtree have values that are
    less than that of the root node.
    - All nodes in the right subtree have values that are
    greater than that of the root node.

    - The property applies to all nodes in the tree.
    """

    def search(root, value):
        """Determine whether an element exists in a BST."""
        if value == root.value:
            return True

        elif root.left and value < root.value:
            return root.left.search(value)

        elif root.right and value > root.value:
            return root.right.search(value)

        else:
            return False

    def insert(root, value):
        """Insert an element into a BST."""
        side = 'left' if value < root.value else 'right'

        child_node = getattr(root, side)
        if not child_node:
            setattr(root, side, BinarySearchTree(value))

        else:
            child_node.insert(value)

        return root

    def delete(root, value):
        """Delete an element from a BST."""
        if root.value == value:
            if root.left:
                root.value = root.left.get_max_value()
                root.left = root.left.delete(root.value)
            else:
                return root.right

        side = 'left' if value < root.value else 'right'

        child_node = getattr(root, side)
        if child_node:
            setattr(root, side, child_node.delete(value))
        else:
            return root

    @classmethod
    def create_bst(cls, elements):
        """Create a BST from a list of elements."""
        if not elements:
            raise Exception('The array of elements is empty.')

        root = cls(elements[0])

        for i in range(1, len(elements)):
            root = root.insert(elements[i])

        return root
