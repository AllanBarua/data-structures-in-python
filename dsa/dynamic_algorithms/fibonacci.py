"""Get nth number in a fibonacci sequence."""


def memoized_fibonacci(n, results_cache):
    """Get the nth fibonacci number.

    The fibonacci sequence has overlapping subproblems,
    we introduce memoization to ensure every subproblem is only
    solved once.
    """
    if n < 3:
        return 1

    if not results_cache.get(n):
        results_cache[n] = memoized_fibonacci(
            n - 1, results_cache) + memoized_fibonacci(n - 2, results_cache)

    return results_cache[n]


def fibonacci(n):
    """Call the recursive function with correct initial arguments."""
    return memoized_fibonacci(n, {})
